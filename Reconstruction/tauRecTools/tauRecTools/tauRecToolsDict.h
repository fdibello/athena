// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//
#ifndef TAURECTOOLS_TAURECTOOLSDICT_H
#define TAURECTOOLS_TAURECTOOLSDICT_H

// Local include(s).
#include "tauRecTools/TauVertexedClusterDecorator.h"
#include "tauRecTools/TauCalibrateLC.h"
#include "tauRecTools/TauCommonCalcVars.h"
#include "tauRecTools/TauSubstructureVariables.h"
#include "tauRecTools/TauRecToolBase.h"
#include "tauRecTools/ITauToolBase.h"
#include "tauRecTools/MvaTESVariableDecorator.h"
#include "tauRecTools/MvaTESEvaluator.h"
#include "tauRecTools/TauTrackRNNClassifier.h"
#include "tauRecTools/TauTrackClassifier.h"
#include "tauRecTools/TauCombinedTES.h"
#include "tauRecTools/TauWPDecorator.h"
#include "tauRecTools/TauJetBDTEvaluator.h"
#include "tauRecTools/TauIDVarCalculator.h"
#include "tauRecTools/TauJetRNNEvaluator.h"
#include "tauRecTools/TauDecayModeNNClassifier.h"

#endif // TAURECTOOLS_TAURECTOOLSDICT_H
