# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

from .PostIncludes import ITkHitAnalysis
 
__all__ = ['ITkHitAnalysis']
