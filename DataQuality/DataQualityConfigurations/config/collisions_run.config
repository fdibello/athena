# ****************************
metadata GitInfo {
  Hash = unknown
}
# ****************************

#######################
# References
#######################

reference CentrallyManagedReferences {
  location = /eos/atlas/atlascerngroupdisk/data-dqm/references/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/
  file = data18_13TeV.00358031.express_express.merge.HIST.f961_h322._0001.1
  path = run_358031
  info = Run 358031, express_express
  #database = collisions
  name = same_name
}

reference CentrallyManagedReferences_Main {
  location = /eos/atlas/atlascerngroupdisk/data-dqm/references/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/
  file = data18_13TeV.00358031.physics_Main.merge.HIST.f961_h322._0001.1
  path = run_358031
  info = Run 358031, physics_Main
  #database = collisions
  name = same_name
}

reference CentrallyManagedReferences_Trigger {
  location = /eos/atlas/atlascerngroupdisk/data-dqm/references/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/
  file = data18_13TeV.00356177.express_express.merge.HIST.f956_h317._0001.1
  path = run_356177
  info = Run 356177, express_express
  #database = collisions
  name = same_name
}

reference CentrallyManagedReferences_TriggerMain {
  location = /eos/atlas/atlascerngroupdisk/data-dqm/references/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/
  file = data18_13TeV.00356177.physics_Main.merge.HIST.f956_h319._0001.1
  path = run_356177
  info = Run 356177, physics_Main
  #database = collisions
  name = same_name
}

reference CentrallyManagedReferences_TriggerBphysLS {
  location = /eos/atlas/atlascerngroupdisk/data-dqm/references/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/
  file = data18_13TeV.00356177.physics_BphysLS.merge.HIST.f961_h322._0001.1
  path = run_356177
  info = Run 356177, physics_BphysLS
  #database = collisions
  name = same_name
}

####################
# Common Algorithms
####################

algorithm WorstCaseSummary {
  libname = libdqm_summaries.so
  name = WorstCaseSummary
}

algorithm Histogram_Empty {
  libname = libdqm_algorithms.so
  name = Histogram_Empty
}

algorithm Histogram_Effective_Empty {
  libname = libdqm_algorithms.so
  name = Histogram_Effective_Empty
}

algorithm Histogram_Not_Empty {
  libname = libdqm_algorithms.so
  name = Histogram_Not_Empty
}

algorithm No_UnderFlows {
  libname = libdqm_algorithms.so
  name = No_UnderFlows
}

algorithm No_OverFlows {
  libname = libdqm_algorithms.so
  name = No_OverFlows
}

algorithm All_Bins_Filled {
  libname = libdqm_algorithms.so
  name = All_Bins_Filled
}

algorithm GatherData {
  libname = libdqm_algorithms.so
  name = GatherData
}

compositeAlgorithm CheckRMS&Histogram_Not_Empty {
  subalgs = CheckHisto_RMS,Histogram_Not_Empty
  libname = libdqm_algorithms.so
}

compositeAlgorithm CheckMean&Histogram_Not_Empty {
  subalgs = CheckHisto_Mean,Histogram_Not_Empty
  libname = libdqm_algorithms.so
}

compositeAlgorithm BinsNotThreshold&Histogram_Not_Empty {
  subalgs = Bins_NotEqual_Threshold,Histogram_Not_Empty
  libname = libdqm_algorithms.so
}

compositeAlgorithm Histogram_Not_Empty&GatherData {
  subalgs = GatherData,Histogram_Not_Empty
  libnames = libdqm_algorithms.so
}

compositeAlgorithm CheckHisto_Mean&GatherData {
  subalgs = GatherData,CheckHisto_Mean
  libnames = libdqm_algorithms.so
}

compositeAlgorithm GatherData&Chi2NDF {
  libnames = libdqm_algorithms.so
  subalgs = GatherData,Chi2Test_Chi2_per_NDF
}

#############
# Output
#############

output top_level {
  algorithm = WorstCaseSummary
  reference = CentrallyManagedReferences
}

/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/
############################################################
# AFP
# Nikola Dikic
############################################################

############################################################
 # Reference
reference AFPBinContentReference {
	 location = /eos/atlas/atlascerngroupdisk/data-dqm/references/Other/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/Other/
	 file = AFPOutput62-337176-500k.root
	 path = run_337176
	 name = same_name
}
############################################################

############################################################
# Output
############################################################

output top_level {
	output AFP {
		output SiT {
			output pixelColRow2D {
				output nearAside {
				}
				output farAside {
				}
				output nearCside {
				}
				output farCside {
				}
			}
			output PixelColIDChip {
				output nearAside {
				}
				output farAside {
				}
				output nearCside {
				}
				output farCside {
				}
			}
			output PixelRowIDChip {
				output nearAside {
				}
				output farAside {
				}
				output nearCside {
				}
				output farCside {
				}
			}
			output SiTimeOverThreshold {
				output nearAside {
				}
				output farAside {
				}
				output nearCside {
				}
				output farCside {
				}
			}
			output HitsPerPlanes {
			}
			output Cluster {
				output nearAside {
				}
				output farAside {
				}
				output nearCside {
				}
				output farCside {
				}
			}
			output Track {
			}
			output clustersPerPlane {
				output nearAside {
				}
				output farAside {
				}
				output nearCside {
				}
				output farCside {
				}
			}
			output clustersPerStation {
			}
			output clustersPerPlaneFront {
				output nearAside {
				}
				output farAside {
				}
				output nearCside {
				}
				output farCside {
				}
			}
			output clustersPerPlaneEnd {
				output nearAside {
				}
				output farAside {
				}
				output nearCside {
				}
				output farCside {
				}
			}
			output clustersPerPlaneMiddle {
				output nearAside {
				}
				output farAside {
				}
				output nearCside {
				}
				output farCside {
				}
			}
		}
		output ToF {
			output HitBarvsTrain {
			}
			output HitsPerBarsInTrain {
				output farAside {
				}
				output farCside {
				}
			}
		}
	}
}

############################################################
# Histogram Assessments
############################################################

dir AFP {
	dir SiT {
		dir pixelColRow2D {
			dir nearAside {
				hist pixelColIDChip_vs_pixelRowIDChip_nearAside_P0 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/nearAside
					display = LogZ,Draw=COLZ
				}
				hist pixelColIDChip_vs_pixelRowIDChip_nearAside_P1 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/nearAside
					display = LogZ,Draw=COLZ
				}
				hist pixelColIDChip_vs_pixelRowIDChip_nearAside_P2 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/nearAside
					display = LogZ,Draw=COLZ
				}
				hist pixelColIDChip_vs_pixelRowIDChip_nearAside_P3 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/nearAside
					display = LogZ,Draw=COLZ
				}
			}
			dir nearCside {
				hist pixelColIDChip_vs_pixelRowIDChip_nearCside_P0 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/nearCside
					display = LogZ,Draw=COLZ
				}
				hist pixelColIDChip_vs_pixelRowIDChip_nearCside_P1 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/nearCside
					display = LogZ,Draw=COLZ
				}
				hist pixelColIDChip_vs_pixelRowIDChip_nearCside_P2 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/nearCside
					display = LogZ,Draw=COLZ
				}
				hist pixelColIDChip_vs_pixelRowIDChip_nearCside_P3 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/nearCside
					display = LogZ,Draw=COLZ
				}
			}
			dir farAside {
				hist pixelColIDChip_vs_pixelRowIDChip_farAside_P0 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/farAside
					display = LogZ,Draw=COLZ
				}
				hist pixelColIDChip_vs_pixelRowIDChip_farAside_P1 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/farAside
					display = LogZ,Draw=COLZ
				}
				hist pixelColIDChip_vs_pixelRowIDChip_farAside_P2 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/farAside
					display = LogZ,Draw=COLZ
				}
				hist pixelColIDChip_vs_pixelRowIDChip_farAside_P3 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/farAside
					display = LogZ,Draw=COLZ
				}
			}
			dir farCside {
				hist pixelColIDChip_vs_pixelRowIDChip_farCside_P0 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/farCside
					display = LogZ,Draw=COLZ
				}
				hist pixelColIDChip_vs_pixelRowIDChip_farCside_P1 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/farCside
					display = LogZ,Draw=COLZ
				}
				hist pixelColIDChip_vs_pixelRowIDChip_farCside_P2 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/farCside
					display = LogZ,Draw=COLZ
				}
				hist pixelColIDChip_vs_pixelRowIDChip_farCside_P3 {
					algorithm = HNE
					output = AFP/SiT/pixelColRow2D/farCside
					display = LogZ,Draw=COLZ
				}
			}
		}
		dir PixelColIDChip {
			dir nearAside {
				hist pixelColIDChip_nearAside_P0 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/nearAside
					display = Draw=HIST
				}
				hist pixelColIDChip_nearAside_P1 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/nearAside
					display = Draw=HIST
				}
				hist pixelColIDChip_nearAside_P2 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/nearAside
					display = Draw=HIST
				}
				hist pixelColIDChip_nearAside_P3 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/nearAside
					display = Draw=HIST
				}
			}
			dir farAside {
				hist pixelColIDChip_farAside_P0 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/farAside
					display = Draw=HIST
				}
				hist pixelColIDChip_farAside_P1 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/farAside
					display = Draw=HIST
				}
				hist pixelColIDChip_farAside_P2 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/farAside
					display = Draw=HIST
				}
				hist pixelColIDChip_farAside_P3 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/farAside
					display = Draw=HIST
				}
			}
			dir nearCside {
				hist pixelColIDChip_nearCside_P0 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/nearCside
					display = Draw=HIST
				}
				hist pixelColIDChip_nearCside_P1 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/nearCside
					display = Draw=HIST
				}
				hist pixelColIDChip_nearCside_P2 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/nearCside
					display = Draw=HIST
				}
				hist pixelColIDChip_nearCside_P3 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/nearCside
					display = Draw=HIST
				}
			}
			dir farCside {
				hist pixelColIDChip_farCside_P0 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/farCside
					display = Draw=HIST
				}
				hist pixelColIDChip_farCside_P1 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/farCside
					display = Draw=HIST
				}
				hist pixelColIDChip_farCside_P2 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/farCside
					display = Draw=HIST
				}
				hist pixelColIDChip_farCside_P3 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelColIDChip/farCside
					display = Draw=HIST
				}
			}
		}
		dir PixelRowIDChip {
			dir nearAside {
				hist pixelRowIDChip_nearAside_P0 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/nearAside
					display = Draw=HIST
				}
				hist pixelRowIDChip_nearAside_P1 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/nearAside
					display = Draw=HIST
				}
				hist pixelRowIDChip_nearAside_P2 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/nearAside
					display = Draw=HIST
				}
				hist pixelRowIDChip_nearAside_P3 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/nearAside
					display = Draw=HIST
				}
			}
			dir farAside {
				hist pixelRowIDChip_farAside_P0 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/farAside
					display = Draw=HIST
				}
				hist pixelRowIDChip_farAside_P1 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/farAside
					display = Draw=HIST
				}
				hist pixelRowIDChip_farAside_P2 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/farAside
					display = Draw=HIST
				}
				hist pixelRowIDChip_farAside_P3 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/farAside
					display = Draw=HIST
				}
			}
			dir nearCside {
				hist pixelRowIDChip_nearCside_P0 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/nearCside
					display = Draw=HIST
				}
				hist pixelRowIDChip_nearCside_P1 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/nearCside
					display = Draw=HIST
				}
				hist pixelRowIDChip_nearCside_P2 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/nearCside
					display = Draw=HIST
				}
				hist pixelRowIDChip_nearCside_P3 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/nearCside
					display = Draw=HIST
				}
			}
			dir farCside {
				hist pixelRowIDChip_farCside_P0 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/farCside
					display = Draw=HIST
				}
				hist pixelRowIDChip_farCside_P1 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/farCside
					display = Draw=HIST
				}
				hist pixelRowIDChip_farCside_P2 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/farCside
					display = Draw=HIST
				}
				hist pixelRowIDChip_farCside_P3 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/PixelRowIDChip/farCside
					display = Draw=HIST
				}
			}
		}
		dir SiTimeOverThreshold {
			dir nearAside {
				hist timeOverThreshold_nearAside_P0 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/nearAside
					display = Draw=HIST
					}
				hist timeOverThreshold_nearAside_P1 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/nearAside
					display = Draw=HIST
				}
				hist timeOverThreshold_nearAside_P2 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/nearAside
					display = Draw=HIST
				}
				hist timeOverThreshold_nearAside_P3 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/nearAside
					display = Draw=HIST
				}
			}
			dir farAside {
				hist timeOverThreshold_farAside_P0 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/farAside
					display = Draw=HIST
				}
				hist timeOverThreshold_farAside_P1 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/farAside
					display = Draw=HIST
				}
				hist timeOverThreshold_farAside_P2 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/farAside
					display = Draw=HIST
				}
				hist timeOverThreshold_farAside_P3 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/farAside
					display = Draw=HIST
				}
			}
			dir nearCside {
				hist timeOverThreshold_nearCside_P0 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/nearCside
					display = Draw=HIST
				}
				hist timeOverThreshold_nearCside_P1 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/nearCside
					display = Draw=HIST
				}
				hist timeOverThreshold_nearCside_P2 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/nearCside
					display = Draw=HIST
				}
				hist timeOverThreshold_nearCside_P3 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/nearCside
					display = Draw=HIST
				}
			}
			dir farCside {
				hist timeOverThreshold_farCside_P0 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/farCside
					display = Draw=HIST
				}
				hist timeOverThreshold_farCside_P1 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/farCside
					display = Draw=HIST
				}
				hist timeOverThreshold_farCside_P2 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/farCside
					display = Draw=HIST
				}
				hist timeOverThreshold_farCside_P3 {
					algorithm = AFPBinContentComp
					output = AFP/SiT/SiTimeOverThreshold/farCside
					display = Draw=HIST
				}
			}
		}
		dir HitsPerPlanes {
			hist planeHitsAll {
				algorithm = HNE
				output = AFP/SiT/HitsPerPlanes
				display = Draw=HIST
			}
			hist planeHits_nearAside {
				algorithm = AFPBinContentComp
				output = AFP/SiT/HitsPerPlanes
				display = Draw=HIST
			}
			hist planeHits_nearCside {
				algorithm = AFPBinContentComp
				output = AFP/SiT/HitsPerPlanes
				display = Draw=HIST
			}
			hist planeHits_farAside {
				algorithm = AFPBinContentComp
				output = AFP/SiT/HitsPerPlanes
				display = Draw=HIST
			}
			hist planeHits_farCside {
				algorithm = AFPBinContentComp
				output = AFP/SiT/HitsPerPlanes
				display = Draw=HIST
			}
		}
		dir Cluster {
			dir nearAside {
				hist clusterX_vs_clusterY_nearAside_P0 {
					algorithm = HNE
					output = AFP/SiT/Cluster/nearAside
					display = LogZ,Draw=COLZ
				}
				hist clusterX_vs_clusterY_nearAside_P1 {
					algorithm = HNE
					output = AFP/SiT/Cluster/nearAside
					display = LogZ,Draw=COLZ
				}
				hist clusterX_vs_clusterY_nearAside_P2 {
					algorithm = HNE
					output = AFP/SiT/Cluster/nearAside
					display = LogZ,Draw=COLZ
				}
				hist clusterX_vs_clusterY_nearAside_P3 {
					algorithm = HNE
					output = AFP/SiT/Cluster/nearAside
					display = LogZ,Draw=COLZ
				}
			}
			dir farAside {
				hist clusterX_vs_clusterY_farAside_P0 {
					algorithm = HNE
					output = AFP/SiT/Cluster/farAside
					display = LogZ,Draw=COLZ
				}
				hist clusterX_vs_clusterY_farAside_P1 {
					algorithm = HNE
					output = AFP/SiT/Cluster/farAside
					display = LogZ,Draw=COLZ
				}
				hist clusterX_vs_clusterY_farAside_P2 {
					algorithm = HNE
					output = AFP/SiT/Cluster/farAside
					display = LogZ,Draw=COLZ
				}
				hist clusterX_vs_clusterY_farAside_P3 {
					algorithm = HNE
					output = AFP/SiT/Cluster/farAside
					display = LogZ,Draw=COLZ
				}
			}
			dir nearCside {
				hist clusterX_vs_clusterY_nearCside_P0 {
					algorithm = HNE
					output = AFP/SiT/Cluster/nearCside
					display = LogZ,Draw=COLZ
				}
				hist clusterX_vs_clusterY_nearCside_P1 {
					algorithm = HNE
					output = AFP/SiT/Cluster/nearCside
					display = LogZ,Draw=COLZ
				}
				hist clusterX_vs_clusterY_nearCside_P2 {
					algorithm = HNE
					output = AFP/SiT/Cluster/nearCside
					display = LogZ,Draw=COLZ
				}
				hist clusterX_vs_clusterY_nearCside_P3 {
					algorithm = HNE
					output = AFP/SiT/Cluster/nearCside
					display = LogZ,Draw=COLZ
				}
			}
			dir farCside {
				hist clusterX_vs_clusterY_farCside_P0 {
					algorithm = HNE
					output = AFP/SiT/Cluster/farCside
					display = LogZ,Draw=COLZ
				}
				hist clusterX_vs_clusterY_farCside_P1 {
					algorithm = HNE
					output = AFP/SiT/Cluster/farCside
					display = LogZ,Draw=COLZ
				}
				hist clusterX_vs_clusterY_farCside_P2 {
					algorithm = HNE
					output = AFP/SiT/Cluster/farCside
					display = LogZ,Draw=COLZ
				}
				hist clusterX_vs_clusterY_farCside_P3 {
					algorithm = HNE
					output = AFP/SiT/Cluster/farCside
					display = LogZ,Draw=COLZ
				}
			}
		}
		dir Track {
			hist trackX_vs_trackY_nearAside {
				algorithm = HNE
				output = AFP/SiT/Track
				display = LogZ,Draw=COLZ
			}
			hist trackX_vs_trackY_nearCside {
				algorithm = HNE
				output = AFP/SiT/Track
				display = LogZ,Draw=COLZ
			}
			hist trackX_vs_trackY_farAside {
				algorithm = HNE
				output = AFP/SiT/Track
				display = LogZ,Draw=COLZ
			}
			hist trackX_vs_trackY_farCside {
				algorithm = HNE
				output = AFP/SiT/Track
				display = LogZ,Draw=COLZ
			}
		}
		dir clustersPerPlane {
			dir farAside {
				hist clustersPerPlane_vs_lb_farAside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/farAside
					display = Draw=COLZ
				}
				hist clustersPerPlane_vs_lb_farAside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/farAside
					display = Draw=COLZ
				}
				hist clustersPerPlane_vs_lb_farAside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/farAside
					display = Draw=COLZ
				}
				hist clustersPerPlane_vs_lb_farAside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/farAside
					display = Draw=COLZ
				}
			}
			dir nearAside {
				hist clustersPerPlane_vs_lb_nearAside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/nearAside
					display = Draw=COLZ
				}
				hist clustersPerPlane_vs_lb_nearAside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/nearAside
					display = Draw=COLZ
				}
				hist clustersPerPlane_vs_lb_nearAside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/nearAside
					display = Draw=COLZ
				}
				hist clustersPerPlane_vs_lb_nearAside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/nearAside
					display = Draw=COLZ
				}
			}
			dir farCside {
				hist clustersPerPlane_vs_lb_farCside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/farCside
					display = Draw=COLZ
				}
				hist clustersPerPlane_vs_lb_farCside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/farCside
					display = Draw=COLZ
				}
				hist clustersPerPlane_vs_lb_farCside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/farCside
					display = Draw=COLZ
				}
				hist clustersPerPlane_vs_lb_farCside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/farCside
					display = Draw=COLZ
				}
			}
			dir nearCside {
				hist clustersPerPlane_vs_lb_nearCside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/nearCside
					display = Draw=COLZ
				}
				hist clustersPerPlane_vs_lb_nearCside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/nearCside
					display = Draw=COLZ
				}
				hist clustersPerPlane_vs_lb_nearCside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/nearCside
					display = Draw=COLZ
				}
				hist clustersPerPlane_vs_lb_nearCside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlane/nearCside
					display = Draw=COLZ
				}
			}
		}
		dir clustersPerPlaneFront {
			dir farAside {
				hist clustersPerPlaneFront_vs_lb_farAside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/farAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneFront_vs_lb_farAside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/farAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneFront_vs_lb_farAside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/farAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneFront_vs_lb_farAside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/farAside
					display = Draw=COLZ
				}
			}
			dir nearAside {
				hist clustersPerPlaneFront_vs_lb_nearAside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/nearAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneFront_vs_lb_nearAside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/nearAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneFront_vs_lb_nearAside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/nearAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneFront_vs_lb_nearAside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/nearAside
					display = Draw=COLZ
				}
			}
			dir farCside {
				hist clustersPerPlaneFront_vs_lb_farCside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/farCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneFront_vs_lb_farCside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/farCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneFront_vs_lb_farCside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/farCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneFront_vs_lb_farCside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/farCside
					display = Draw=COLZ
				}
			}
			dir nearCside {
				hist clustersPerPlaneFront_vs_lb_nearCside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/nearCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneFront_vs_lb_nearCside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/nearCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneFront_vs_lb_nearCside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/nearCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneFront_vs_lb_nearCside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneFront/nearCside
					display = Draw=COLZ
				}
			}
		}
		dir clustersPerPlaneEnd {
			dir farAside {
				hist clustersPerPlaneEnd_vs_lb_farAside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/farAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneEnd_vs_lb_farAside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/farAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneEnd_vs_lb_farAside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/farAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneEnd_vs_lb_farAside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/farAside
					display = Draw=COLZ
				}
			}
			dir nearAside {
				hist clustersPerPlaneEnd_vs_lb_nearAside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/nearAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneEnd_vs_lb_nearAside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/nearAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneEnd_vs_lb_nearAside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/nearAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneEnd_vs_lb_nearAside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/nearAside
					display = Draw=COLZ
				}
			}
			dir farCside {
				hist clustersPerPlaneEnd_vs_lb_farCside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/farCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneEnd_vs_lb_farCside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/farCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneEnd_vs_lb_farCside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/farCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneEnd_vs_lb_farCside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/farCside
					display = Draw=COLZ
				}
			}
			dir nearCside {
				hist clustersPerPlaneEnd_vs_lb_nearCside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/nearCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneEnd_vs_lb_nearCside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/nearCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneEnd_vs_lb_nearCside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/nearCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneEnd_vs_lb_nearCside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneEnd/nearCside
					display = Draw=COLZ
				}
			}
		}
		dir clustersPerPlaneMiddle {
			dir farAside {
				hist clustersPerPlaneMiddle_vs_lb_farAside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/farAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneMiddle_vs_lb_farAside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/farAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneMiddle_vs_lb_farAside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/farAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneMiddle_vs_lb_farAside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/farAside
					display = Draw=COLZ
				}
			}
			dir nearAside {
				hist clustersPerPlaneMiddle_vs_lb_nearAside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/nearAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneMiddle_vs_lb_nearAside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/nearAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneMiddle_vs_lb_nearAside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/nearAside
					display = Draw=COLZ
				}
				hist clustersPerPlaneMiddle_vs_lb_nearAside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/nearAside
					display = Draw=COLZ
				}
			}
			dir farCside {
				hist clustersPerPlaneMiddle_vs_lb_farCside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/farCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneMiddle_vs_lb_farCside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/farCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneMiddle_vs_lb_farCside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/farCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneMiddle_vs_lb_farCside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/farCside
					display = Draw=COLZ
				}
			}
			dir nearCside {
				hist clustersPerPlaneMiddle_vs_lb_nearCside_P0 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/nearCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneMiddle_vs_lb_nearCside_P1 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/nearCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneMiddle_vs_lb_nearCside_P2 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/nearCside
					display = Draw=COLZ
				}
				hist clustersPerPlaneMiddle_vs_lb_nearCside_P3 {
					algorithm = HNE
					output = AFP/SiT/clustersPerPlaneMiddle/nearCside
					display = Draw=COLZ
				}
			}
		}
		dir clustersPerStation {
			hist clustersPerStation_vs_lb_farAside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStation_vs_lb_farCside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStation_vs_lb_nearAside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStation_vs_lb_nearCside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStationFront_vs_lb_farAside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStationFront_vs_lb_farCside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStationFront_vs_lb_nearAside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStationFront_vs_lb_nearCside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStationEnd_vs_lb_farAside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStationEnd_vs_lb_farCside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStationEnd_vs_lb_nearAside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStationEnd_vs_lb_nearCside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStationMiddle_vs_lb_farAside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStationMiddle_vs_lb_farCside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStationMiddle_vs_lb_nearAside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
			hist clustersPerStationMiddle_vs_lb_nearCside {
				algorithm = HNE
				output = AFP/SiT/clustersPerStation
				display = Draw=COLZ
			}
		}
		hist nSiHits_vs_lb {
			algorithm = HNE
			output = AFP/SiT
			display = Draw=HIST
		}
		hist muPerBCID_vs_lb {
			algorithm = HNE
			output = AFP/SiT
			display = Draw=HIST
		}
	}
	dir ToF {
		hist numberOfHit_S0 {
			algorithm = HNE
			output = AFP/ToF
			display = Draw=HIST
		}
		hist numberOfHit_S3 {
			algorithm = HNE
			output = AFP/ToF
			display = Draw=HIST
		}
		dir HitBarvsTrain {
			hist trainID_vs_barInTrainID_farAside {
				algorithm = HNE
				output = AFP/ToF/HitBarvsTrain
				display = Draw=COLZ
			}
			hist trainID_vs_barInTrainID_farCside {
				algorithm = HNE
				output = AFP/ToF/HitBarvsTrain
				display = Draw=COLZ
			}
		}
		dir HitsPerBarsInTrain {
			hist barInTrainAllA {
				algorithm = HNE
				output = AFP/ToF/HitsPerBarsInTrain
				display = Draw=COLZ
			}
			hist barInTrainAllC {
				algorithm = HNE
				output = AFP/ToF/HitsPerBarsInTrain
				display = Draw=COLZ
			}
			dir farAside {
				hist barInTrainIDA_train0 {
					algorithm = AFPBinContentComp
					output = AFP/ToF/HitsPerBarsInTrain/farAside
					display = Draw=COLZ
				}
				hist barInTrainIDA_train1 {
					algorithm = AFPBinContentComp
					output = AFP/ToF/HitsPerBarsInTrain/farAside
					display = Draw=COLZ
				}
				hist barInTrainIDA_train2 {
					algorithm = AFPBinContentComp
					output = AFP/ToF/HitsPerBarsInTrain/farAside
					display = Draw=COLZ
				}
				hist barInTrainIDA_train3 {
					algorithm = AFPBinContentComp
					output = AFP/ToF/HitsPerBarsInTrain/farAside
					display = Draw=COLZ
				}
			}
			dir farCside {
				hist barInTrainIDC_train0 {
					algorithm = AFPBinContentComp
					output = AFP/ToF/HitsPerBarsInTrain/farCside
					display = Draw=COLZ
				}
				hist barInTrainIDC_train1 {
					algorithm = AFPBinContentComp
					output = AFP/ToF/HitsPerBarsInTrain/farCside
					display = Draw=COLZ
				}
				hist barInTrainIDC_train2 {
					algorithm = AFPBinContentComp
					output = AFP/ToF/HitsPerBarsInTrain/farCside
					display = Draw=COLZ
				}
				hist barInTrainIDC_train3 {
					algorithm = AFPBinContentComp
					output = AFP/ToF/HitsPerBarsInTrain/farCside
					display = Draw=COLZ
				}
			}
		}
	}
}

############################################################
# Algorithms

algorithm HNE {
	libname = libdqm_algorithms.so
	name = Histogram_Not_Empty
}

algorithm AFPBinContentComp {
	libname = libdqm_algorithms.so
	name = BinContentComp
	thresholds = AFPTestThreshold
	NSigma = 3
	reference = AFPBinContentReference
	publish = 4
	NormRef = 1
}
############################################################


############################################################
# Thresholds
thresholds AFPTestThreshold {
	limits NBins {
		warning = 1
		error = 2
	}
}
###########################################################

